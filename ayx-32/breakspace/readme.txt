
Digitized on ZX-Evolution.
192kHz, 16 bit, stereo

ym.wav
  - YM-2149F chip

ayx-ym-fs+4db.wav
  - AYX-32 with YM amplitude table,
  - full stereo ABC (64/0, 32/32, 0/64),
  - volume increased by 4dB to coerce to ym.wav mean volume
